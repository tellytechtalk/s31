// a. What directive is used by Node.js in loading the modules it needs?

 answer: "Require" directive
 let http = require("http")


// b. What Node.js module contains a method for server creation?

answer: "http module "


// c. What is the method of the http object responsible for creating a server using Node.js?

answer: createServer() method
http.createServer(function(request,response){


// d. What method of the response object allows us to set status codes and content types?

answer: "response.writeHead method" 
 response. writeHead(statusCode[, statusMessage][, headers]);


// e. Where will console.log() output its contents when run in Node.js?

answer: "localhost" or "browser"
console.log("Server is running on localHost:4000")


// f. What property of the request object contains the address' endpoint?/

answer: "(request.url) contains the endpoint"